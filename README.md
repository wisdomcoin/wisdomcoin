WisdomCoin integration/staging tree
================================

http://www.wisdomcoin.co

Copyright (c) 2009-2014 Bitcoin Developers
Copyright (c) 2011-2014 Litecoin Developers
Copyright (c) 2015-2018 WisdomCoin Developers



What is WisdomCoin?
----------------
WisdomCoin is a lite version of Litecoin using scrypt as a proof-of-work algorithm.

Ticker: WDC

Denarius [WDC] is an anonymous, untraceable, energy efficient, Proof-of-Work (Scrypt Algorithm) and Proof-of-Stake cryptocurrency.

27,000,000 WDC will be created in about 4 years during the PoW phase. Denarius has a 50% Premine of 27,000,000 WDC for bounties, distribution, and marketing.

For more information, as well as an immediately useable, binary version of
the WisdomCoin client sofware, see http://www.wisdomcoin.co.

Specifications
-------
Total number of coins: 27,000,000 WDC
Ideal block time: 30 seconds
Confirmations: 10 blocks
Maturity: 30 blocks (15 minutes)
Min stake age: 8 hours


Technology
-------

Stealth addresses
Encrypted Messaging
Multi-Signature Addresses & TXs
Fast 30 Second Block Times
Scrypt PoW Algorithm 
Full decentralization

Download Wallet 
-------------------

HOW TO SET UP WISDOMCOIN WALLET ON WINDOWS OPERATING SYSTEM?

A “WisDomcoin wallet” is basically the WisDomcoin Coin Account, which allows you to receive WisDomcoin Coin, store them, and then send them to others.

Click here : http://www.wisdomcoin.co/wallet/wisdomcoin-wallet.zip

Unzip the wallets files.
You will get wisdomcoin.exe file, Install the wallet software by double click on wisdomcoin.exe file.
You can now send and receive WisDomcoin Coin directly from WisDomcoin Desktop Wallet and also use this wallet to stake WisDomcoin Coin.

HOW TO SET UP WISDOMCOIN WALLET ON LINUX OPERATING SYSTEM?

Click here : http://www.wisdomcoin.co/wallet/wisdomcoin-linux-qt.zip

Open linux terminal and go to destination path of downloaded directory.
Unzip the wallets files using command unzip wallet_file.zip -d destination_folder.
execute the wallet file using command ./wisdomcoin-linux-qt.

Unzip the wallets files.
You will get wisdomcoin-qt.dmg file, Install the wallet software by double click on wisdomcoin-qt.dmg file.
You can now send and receive WisDomcoin Coin directly from WisDomcoin Desktop Wallet and also use this wallet to stake WisDomcoin Coin.

